#ifndef CORDICCART2POL_H
#define CORDICCART2POL_H

#include "ap_fixed.h"
#include "ap_int.h"

#define NO_ITER 16

typedef ap_fixed<16, 3, AP_RND, AP_WRAP, 1> data_t;
typedef ap_fixed<32, 3, AP_RND, AP_WRAP, 1> angles_t;
typedef ap_int<2> sigma_t;
typedef ap_uint<5> iter_t;

void cordiccart2pol(data_t x, data_t y, data_t *r,  data_t *theta);

#endif
