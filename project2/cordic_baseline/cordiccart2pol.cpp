#include "cordiccart2pol.h"

data_t Kvalues[NO_ITER] = {1,	0.500000000000000,	0.250000000000000,	0.125000000000000,	0.0625000000000000,	0.0312500000000000,	0.0156250000000000,	0.00781250000000000,	0.00390625000000000,	0.00195312500000000,	0.000976562500000000,	0.000488281250000000,	0.000244140625000000,	0.000122070312500000,	6.10351562500000e-05,	3.05175781250000e-05};
data_t angles[NO_ITER] = {0.785398163397448,	0.463647609000806,	0.244978663126864,	0.124354994546761,	0.0624188099959574,	0.0312398334302683,	0.0156237286204768,	0.00781234106010111,	0.00390623013196697,	0.00195312251647882,	0.000976562189559320,	0.000488281211194898,	0.000244140620149362,	0.000122070311893670, 6.10351561742088e-05, 3.05175781155261e-05};


void cordiccart2pol(data_t x, data_t y, data_t *r,  data_t *theta)
{
	// Write your code here

	data_t x_i;
	data_t acc = 0.0;
	data_t A = 0.607252935103141;	// Inverse Gain factor for NO_ITER = 16
	data_t rad90 = 1.570796326797900;	// 90degrees -> radians

	// Rotate by 90 degrees to ensure vector is is Q-I or Q-IV
	// Determine direction of rotation
	int sigma = y < 0 ? 1:-1;

	// Update x and y values
	x_i = x;
	x = -sigma*y;
	y = sigma*x_i;

	// Update rotation angle (theta)
	acc -= sigma*rad90;

	int i = 0;
	// Iterate successive vector approximations
	for (i = 0; i < NO_ITER; i++) {

		// Determine direction of rotation
		sigma = y < 0 ? 1:-1;

		// Update x and y values
		x_i = x;
		x = x - sigma*Kvalues[i]*y;
		y = y + sigma*Kvalues[i]*x_i;

		// Update rotation angle (theta)
		acc -= sigma*angles[i];
	}
	// Adjust r for gain
	*r = A*x;

	*theta = acc;
}
