#include<math.h>
#include "dft.h"
#include"coefficients256.h"
#include<stdio.h>

DTYPE calculate_R(int i, DTYPE IN_R[SIZE], DTYPE IN_I[SIZE]) {
	DTYPE c, s, ret_r;
	ret_r = 0;
	int j, z;
	for (j=0; j < SIZE; j++) {
		z = (i * j) % SIZE;
		c = cos_coefficients_table[z];
		s = sin_coefficients_table[z];
		ret_r += (IN_R[j] * c - IN_I[j] * s);
	}
	return ret_r;
}

DTYPE calculate_I(int i, DTYPE IN_R[SIZE], DTYPE IN_I[SIZE]) {
	DTYPE c, s, ret_i;
	ret_i = 0;
	int j, z;
	for (j=0; j < SIZE; j++) {
		z = (i * j) % SIZE;
		c = cos_coefficients_table[z];
		s = sin_coefficients_table[z];
		ret_i += (IN_I[j] * c + IN_R[j] * s);
	}
	return ret_i;
}

void dft(DTYPE IN_R[SIZE], DTYPE IN_I[SIZE],
		DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]) {
	int i, j, z;
	DTYPE w, c, s;
	// Calculate each frequency domain sample iteratively
	for (i = 0; i < SIZE; i += 1) {
		OUT_R[i] = calculate_R(i, IN_R, IN_I);
		OUT_I[i] = calculate_I(i, IN_R, IN_I);
	}
}

