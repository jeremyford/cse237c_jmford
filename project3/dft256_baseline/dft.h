
typedef float DTYPE;		// Data type for inputs, outputs, coefficients

#define SIZE 256 			// SIZE OF DFT

void dft(DTYPE XX_R[SIZE], DTYPE XX_I[SIZE]);

