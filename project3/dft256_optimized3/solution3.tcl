############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
set_directive_array_partition -type block -factor 4 -dim 1 "dft" IN_R
set_directive_array_partition -type block -factor 4 -dim 1 "dft" IN_I
set_directive_array_partition -type block -factor 4 -dim 1 "dft" OUT_R
set_directive_array_partition -type block -factor 4 -dim 1 "dft" OUT_I
set_directive_array_partition -type block -factor 4 -dim 1 "dft" cos_coefficients_table
set_directive_array_partition -type block -factor 4 -dim 1 "dft" sin_coefficients_table
