#include<math.h>
#include "dft.h"
#include"coefficients256.h"
#include<stdio.h>

void dft(DTYPE IN_R[SIZE], DTYPE IN_I[SIZE],
		DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]) {
	int i, j, z;
	DTYPE w, c, s;

	// Calculate each frequency domain sample iteratively
	for (i = 0; i < SIZE; i += 1) {
		// Calculate the jth frequency sample sequentially
		for (j = 0; j < SIZE; j += 1) {
			z = (i * j) % SIZE;
			c = cos_coefficients_table[z];
			s = sin_coefficients_table[z];

			// Multiply the current phasor with the appropriate input sample and keep
			// running sum
			OUT_R[i] += (IN_R[j] * c - IN_I[j] * s);
			OUT_I[i] += (IN_I[j] * c + IN_R[j] * s);

		}
	}
}

