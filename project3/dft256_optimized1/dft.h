
typedef float DTYPE;		// Data type for the input, output, and coefficients

#define SIZE 256 			// SIZE OF DFT

void dft(DTYPE XX_R[SIZE], DTYPE XX_I[SIZE]);

