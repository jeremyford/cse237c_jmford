#include <math.h> 				//Required for cos and sin functions
#include "dft.h"				//
#include "coefficients256.h"	//

void dft(DTYPE sample_real[SIZE], DTYPE sample_imag[SIZE]) {
	int i, j;
	DTYPE c, s;

	// Temporary arrays to hold the intermediate frequency domain results
	DTYPE temp_real[SIZE];
	DTYPE temp_imag[SIZE];

	// Calculate each frequency domain sample iteratively
	for (i = 0; i < SIZE; i++) {
		temp_real[i] = 0;
		temp_imag[i] = 0;

		// Calculate the jth frequency sample sequentially
		for (j = 0; j < SIZE; j++) {
			// Utilize HLS tool to calculate sine and cosine values
			c = cos_coefficients_table[(i*j) % SIZE];
			s = sin_coefficients_table[(i*j) % SIZE];

			// Multiply the current phasor with the appropriate input sample and keep running sum
			temp_real[i] += (sample_real[j] * c - sample_imag[j] * s);
			temp_imag[i] += (sample_real[j] * s + sample_imag[j] * c);
		}
	}

// Perform an inplace DFT, i.e., copy result into the input arrays
	for (i = 0; i < SIZE; i++) {
		sample_real[i] = temp_real[i];
		sample_imag[i] = temp_imag[i];
	}
}
