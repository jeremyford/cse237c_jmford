typedef float DTYPE;
#define SIZE 1024		/* SIZE OF DFT */
void dft(DTYPE IN_R[SIZE], DTYPE IN_I[SIZE], DTYPE OUT_R[SIZE],
		DTYPE OUT_I[SIZE]);

