/*
	Filename: fir.h
		Header file
		FIR lab written for WES/CSE237C class at UCSD.

*/
#ifndef FIR_H_
#define FIR_H_

const int N=128;

// optimization1: bit-widths
#include "ap_int.h"
typedef ap_int<5>	  coef_t;
typedef ap_int<8>	  data_t;
typedef ap_int<16>	acc_t;

void fir (
  data_t *y,
  data_t x
  );

#endif
